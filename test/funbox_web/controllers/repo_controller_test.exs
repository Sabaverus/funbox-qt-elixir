defmodule FunboxWeb.RepoControllerTest do
  use FunboxWeb.ConnCase

  describe "index" do
    test "lists github resouces", %{conn: conn} do
      conn = get(conn, Routes.repos_path(conn, :index))
      assert html_response(conn, 200) =~ "Content"
    end
  end
end
