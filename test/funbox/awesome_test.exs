defmodule Funbox.AwesomeTest do
  use Funbox.DataCase

  alias Funbox.Awesome

  describe "repos" do
    alias Funbox.Awesome.Resources.Resource
    alias Funbox.Awesome.Resources.ResourceSection

    @valid_resource_section %{
      title: "Resource section",
      description: "Description", 
      code: "resource_section"
    }
    @invalid_resource_section %{
      title: nil,
      description: nil,
      code: nil
    }

    @valid_resource %{
      title: "some title",
      path: "http://some.com/",
      stars: 42
    }
    @update_resource %{
      title: "some updated title",
      path: "http://some.com/foo/",
      stars: 43
    }
    @invalid_resource %{
      section: nil,
      path: nil,
      stars: nil,
      title: nil
    }

    def resource_section_fixture(attrs \\ %{}) do
      attrs
      |> Enum.into(@valid_resource_section)
      |> Awesome.create_resource_section()
    end

    def resource_fixture(attrs \\ %{}) do
      attrs
      |> Enum.into(@valid_resource)
      |> Awesome.create_resource()
    end

    test "create_resource_section/1 with valid data" do
      assert {:ok, %ResourceSection{} = section} = Awesome.create_resource_section(@valid_resource_section)
      assert section.title == "Resource section"
      assert section.description == "Description"
      assert section.code == "resource_section"
    end

    test "create_repo/1 with valid data creates a repo" do
      assert {:ok, section} = resource_section_fixture()
      assert {:ok, %Resource{} = created} = Awesome.create_resource(@valid_resource)
      assert created.title == "some title"
      assert created.path == "http://some.com/"
      assert created.stars == 42
    end

    test "update_resource/2 with valid data updates the resource" do
      assert {:ok, %Resource{} = resource} = resource_fixture()
      assert {:ok, %ResourceSection{} = section} = resource_section_fixture()
      assert {:ok, %Resource{} = updated} = Awesome.update_resource(resource, Map.put(@update_resource, :section, section.id))
      assert updated.title == "some updated title"
      assert updated.path == "http://some.com/foo/"
      assert updated.section == section.id
      assert updated.stars == 43
    end

    test "create_resource/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Awesome.create_resource(@invalid_resource)
    end

    test "update_resource/2 with invalid data returns error changeset" do
      assert {:ok, %Resource{} = resource} = resource_fixture()
      assert {:error, %Ecto.Changeset{}} = Awesome.update_resource(resource, @invalid_resource)
      assert resource == Awesome.get_resource!(resource.id)
    end

    test "delete_resource/1 deletes the resource" do
      assert {:ok, %Resource{} = resource} = resource_fixture()
      assert {:ok, %Resource{}} = Awesome.delete_resource(resource)
      assert_raise Ecto.NoResultsError, fn -> Awesome.get_resource!(resource.id) end
    end

    test "change_resource/1 returns a resource changeset" do
      assert {:ok, %Resource{} = resource} = resource_fixture()
      assert %Ecto.Changeset{} = Awesome.change_resource(resource)
    end
  end
end
