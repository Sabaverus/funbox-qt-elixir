defmodule Funbox.Awesome.Resources.ResourceSection do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Query, warn: false
  import Ecto.Changeset

  schema "repo_sections" do
    field :title, :string
    field :description, :string
    field :code, :string
    has_many :elements, Funbox.Awesome.Resources.Resource, foreign_key: :section

    timestamps()
  end

  @doc false
  def changeset(repo_section, attrs) do
    repo_section
    |> cast(attrs, [:title, :description, :code])
    |> validate_required([:title])
    |> unique_constraint(:code)
  end

  def by_code(query, code) do
    where(query, [section], section.code == ^code)
  end

  def left_join_resources(query) do
    join(query, :left, [section], element in assoc(section, :elements), as: :element)
  end

  def preload_resources(query) do
    preload(query, [element: element], elements: element)
  end

  def sort_by_title(query, :asc), do: order_by(query, [section], asc: section.title)
  def sort_by_title(query, :desc), do: order_by(query, [section], desc: section.title)
  def sort_by_title(query, _), do: sort_by_title(query, :asc)
end
