defmodule Funbox.Awesome.Resources.Resource do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Query, warn: false
  import Ecto.Changeset

  schema "repos" do
    field :path, :string
    field :description, :string
    field :stars, :integer
    field :type, :integer
    field :last_commit_date, :utc_datetime
    field :fetch_time, :utc_datetime
    field :title, :string
    field :section, :integer
    field :error_code, :integer

    timestamps()
  end

  @doc false
  def changeset(repo, attrs) do
    repo
    |> cast(attrs, [
      :title,
      :path,
      :type,
      :section,
      :description,
      :stars,
      :last_commit_date,
      :fetch_time,
      :error_code
    ])
    |> validate_required([:title, :path])
    |> validate_url(:path)
    |> unique_constraint(:title)
  end

  def validate_url(changeset, field, opts \\ []) do
    validate_change changeset, field, fn _, value ->
      case URI.parse(value) do
        %URI{scheme: nil} -> "is missing a scheme (e.g. https)"
        %URI{host: nil} -> "is missing a host"
        %URI{host: host} ->
          case :inet.gethostbyname(Kernel.to_charlist host) do
            {:ok, _} -> nil
            {:error, _} -> "invalid host"
          end
      end
      |> case do
        error when is_binary(error) -> [{field, Keyword.get(opts, :message, error)}]
        _ -> []
      end
    end
  end

  def fetch_time_older(query, time) do
    where(query, [element], element.fetch_time < ^time and not is_nil(element.fetch_time))
  end

  def fetch_time(query, time) do
    where(query, [element], element.fetch_time == ^time)
  end

  def exclude_with_errors(query) do
    where(query, [element], is_nil(element.error_code))
  end

  def get_github_resouses(query) do
    with_type(query, 1)
  end

  def with_type(query, type) do
    where(query, [element], element.type == ^type)
  end

  # Founded in https://elixirforum.com/t/dynamic-clauses-with-ecto-query/22039/3
  def set_filters(query, filters) do
    Enum.reduce(filters, query, fn
      {:>, field, value}, query ->
        where(query, [element: element], field(element, ^field) > ^value)

      {:<, field, value}, query ->
        where(query, [element: element], field(element, ^field) < ^value)

      {:==, field, value}, query ->
        where(query, [element: element], field(element, ^field) == ^value)

      {_, _, _}, _ ->
        nil
    end)
  end
end
