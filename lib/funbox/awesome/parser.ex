defmodule Funbox.Awesome.Parser do
  @moduledoc """

  The Awesome list
  https://github.com/h4cc/awesome-elixir
  parser
  """

  import Earmark.Parser

  alias Funbox.Repo, as: DB
  alias Funbox.Awesome.Resources.ResourceSection, as: ResourceSection
  alias Funbox.Awesome.Parser.Structs.Element, as: RepoElement
  alias Funbox.Awesome.Parser.Structs.Section, as: RepoSection

  require Logger

  @doc """

  """
  def parse_content(content) do
    {blocks, _links, _options} = Earmark.Parser.parse(String.split(content, ~r{\r\n?|\n}))
    blocks
  end

  @doc """
  Accept as first argument array of parsed lines and returns array of groups
  """
  def process_content(content) do
    # Awesome Elixir
    [%Earmark.Block.Heading{} | content] = content

    # A curated list of amazingly awesome Elixir libraries, resources, and shiny things inspired by awesome-php.
    [_introduction | content] = content

    # If you think a package should be added, please add a 👍 (:+1:) at the according issue or create a new one.
    [_plusone | content] = content

    # There are other sites with curated lists of elixir packages which you can have a look at.
    [_other_curated_lists | content] = content

    # - [Awesome Elixir](#awesome-elixir) and deeper content
    # Goin into variable tableOfContent
    [%Earmark.Block.List{blocks: _} | blocks_list] = content

    # Parse the main content
    iterate_content(blocks_list, nil, [], [])
  end

  defp get_repo_info([text | _]) do
    if RepoElement.is_element?(text) do
      RepoElement.parse_markdown_text(text)
    end
  end

  defp get_repo_info(_), do: Logger.debug("Undefined behaviour in get_repo_info")

  @doc """

  """
  def iterate_content([block | tail], group, items, groups) do
    case block do
      # Ignore this group
      %Earmark.Block.Heading{level: 1} ->
        iterate_content(tail, nil, [], [RepoSection.add_items(group, items) | groups])

      # This header means start of group
      %Earmark.Block.Heading{level: 2} ->
        sgroup = RepoSection.new(block.content)

        if group do
          # If previous group defined, add them items and push to list of groups
          # then define new group
          iterate_content(tail, sgroup, [], [RepoSection.add_items(group, items) | groups])
        else
          iterate_content(tail, sgroup, [], groups)
        end

      %Earmark.Block.Para{} ->
        if group do
          iterate_content(tail, RepoSection.add_description(group, block.lines), items, groups)
        else
          iterate_content(tail, group, items, groups)
        end

      # Iterating over <ul> elements
      %Earmark.Block.List{type: :ul} ->
        iterate_content(tail, group, collect_group_repos(block.blocks, []), groups)

      true ->
        Logger.debug("Found undefined block at #{block.lbn} line")
        iterate_content(tail, group, items, groups)
    end
  end

  # Exit
  def iterate_content(_, group, items, groups) do
    # Dont forget to add last group if exists
    if group do
      [RepoSection.add_items(group, items) | groups]
    end

    groups
  end

  defp collect_group_repos([%Earmark.Block.ListItem{} = block | tail], lines) do
    collect_group_repos(tail, collect_group_repos(block.blocks, lines))
  end

  defp collect_group_repos([%Earmark.Block.Para{} = block | tail], lines) do
    repo = get_repo_info(block.lines)

    if repo do
      collect_group_repos(tail, [repo | lines])
    else
      Logger.debug("Undefined repo")
    end
  end

  defp collect_group_repos([], lines), do: lines

  defp push_section([%RepoSection{} = section | sections], added_sections) do
    code =
      section.title
      |> String.replace(section.title, ~r/[^\w\d]+/, "_")
      |> String.trim("_")
      |> String.downcase()

    description = String.trim(section.description, "*")

    changeset =
      ResourceSection.changeset(
        %ResourceSection{},
        %{
          title: section.title,
          description: description,
          code: code
        }
      )

    case Funbox.Awesome.get_resource_section_by_code(code) do
      # !!!! @todo remove tail operator
      [%ResourceSection{} = exists | _] ->
        push_element(exists, section.items, [])
        push_section(sections, [exists | added_sections])

      _ ->
        result = DB.insert(changeset)

        case result do
          {:ok, model} ->
            push_element(model, section.items, [])

          {:error, changeset} ->
            Logger.error("Error while adding new section #{inspect(changeset)}")
        end

        push_section(sections, [result | added_sections])
    end
  end

  defp push_section(_, added_sections), do: added_sections

  defp push_element(section, [%RepoElement{} = element | items], added_elements) do
    result =
      Funbox.Awesome.create_resource(%{
        title: element.title,
        description: element.description,
        path: element.path,
        type: element.type,
        section: section.id
      })

    push_element(section, items, [result | added_elements])
  end

  defp push_element(_, _, added_elements), do: added_elements

  def push_parsed_content(content), do: push_section(content, [])
end
