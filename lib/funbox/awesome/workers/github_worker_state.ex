defmodule Funbox.Awesome.Workers.GithubWorkerState do
  @moduledoc """
    # cnt: 0, # Сколько на текущий момент было выполнено запросов в период :limit_ms
    # lrt: :os.system_time(:millisecond), # Last request time
    # queue: [] # Очередь репозиториев на обновление
  """

  defstruct [:cnt, :lrt, :queue]

  @doc """
    Set up current count for state witch used in 
  """
  def set_count(%Funbox.Awesome.Workers.GithubWorkerState{} = state, count) do
    %{state | cnt: count}
  end

  @doc """

  """
  def set_last_request_time(%Funbox.Awesome.Workers.GithubWorkerState{} = state, time) do
    %{state | lrt: time}
  end
end
