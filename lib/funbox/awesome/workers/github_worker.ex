defmodule Funbox.Awesome.Workers.GithubWorker do
  @moduledoc """
    Данный "монитор" изначально стал писаться с условием того,
    что монитор может быть лимитирован количеством запросов в
    какой-то период времени.
    Но пришел к тому, что это ограничение не нужно, а удалять... жалко.
    И теперь, например, лимиты могут быть использованы
    когда будет множество асинхронных мониторов. 

    Основная логика сервера в том, что он может принять синхронный запрос 
    на обновление элемента (репозитория), положить его в очередь
    А под капотом крутить рекурсивный вызов функции loop/2, которая может быть
    лимитирована значением таймаута @loop_threshold или вторым параметром функции
  """

  use GenServer

  # Лимит количества запросов. 0 - без лимита
  @limit_cnt 0
  # Лимит ограничения запросов в заданный период. 0 - без лимита
  @limit_ms 0
  # Задержка между итерациями цикла монитора
  @loop_threshold 100

  alias Funbox.Awesome.Workers.GithubWorkerState, as: WorkerState

  require Logger

  def start_link(name \\ nil) do
    GenServer.start_link(
      __MODULE__,
      %WorkerState{
        cnt: 0,
        lrt: :os.system_time(:millisecond),
        queue: []
      },
      name: name
    )
  end

  def init(state) do
    loop(state)
    {:ok, state}
  end

  
  # This call can be used as call from process_queue
  # Example; process_queue fon updating element got RateLimit error from github
  # And need goin to sleep before limit expires
  defp loop(%WorkerState{} = state), do: loop(state, nil)

  defp loop({state, timeout}) when not is_nil(timeout), do: state |> loop(timeout)

  defp loop(%WorkerState{} = state, timeout) do
    Process.send_after(self(), {:work, state}, timeout || @loop_threshold)
    {:noreply, state}
  end

  @doc """
  Accepts new Resourse for update
  resourse must be child of ...Resouses.Resourse, witch type is == 1 and hit path started
  with http://github.com/
  """
  def handle_call({:task, element}, _from, %WorkerState{} = state) do
    {:reply, :ok, %{state | queue: [element | state.queue]}}
  end

  def handle_call({:task, _}, _from, state) do
    {:reply, :error, state}
  end

  def handle_call({:get_queue}, _from, %WorkerState{} = state) do
    {:reply, state.queue, state}
  end

  def handle_info({:work, _}, %WorkerState{} = state)
      when @limit_cnt > 0 and @limit_ms > 0 do
    now = :os.system_time(:millisecond)
    diff = now - state.lrt

    if @limit_ms <= diff do
      new_state = WorkerState.set_last_request_time(state, now)

      if new_state.queue !== [] do
        new_state
        |> WorkerState.set_count(1)
        |> process_queue()
        |> loop()
      else
        loop(new_state)
      end
    else
      if state.queue !== [] do
        if state.cnt < @limit_cnt do
          state
          |> WorkerState.set_count(state.cnt + 1)
          |> process_queue()
          |> loop()
        else
          # Wakeup myself when limit expires
          loop(state, @limit_ms - diff)
        end
      else
        loop(state)
      end
    end
  end

  def handle_info({:noreply, _}, state), do: state

  def handle_info({:work, _}, %WorkerState{} = state) do
    process_queue(state)
    |> loop()
  end

  def handle_info({:timeout, _}, %WorkerState{} = state) do
    log("Got timeout in handle_info", :debug)
    loop(state)
  end

  def process_queue(%WorkerState{} = state) do
    api = "https://api.github.com/repos"
    {element, head} = List.pop_at(state.queue, -1)

    with element when not is_nil(element) <- element,
         [path | _] <- get_repo_path_from_uri(element.path),
         # Get github data for given element
         # Repository data
         {:ok, repo_info, api_repos_response} <- send_request(api <> path),
         # Last commit from master branch
         {:ok, repo_commit_info, _api_commit_response} <-
           send_request(api <> path <> "/commits/master"),
         {:ok, _result} <-
           update_element(element, repo_info, repo_commit_info, api_repos_response) do
      %{state | queue: head}
    else
      {:error, message, _result} ->
        log(
          "Absorbed error in process_queue/1 #{message} on element.id #{element.id}, element.path #{
            element.path
          }",
          :error
        )

        %{state | queue: head}

      {:not_found, message, result} ->
        log("#{message} #{result.request_url}", :debug)
        Funbox.Awesome.update_repo(element, %{error_code: 404})
        %{state | queue: head}

      {:forbidden, _body, result} ->
        # Supposably we got header {"X-RateLimit-Remaining", "0"}
        limits = get_github_limits_from_headers(result.headers)

        if limits[:remain] && limits[:remain] == 0 && limits[:reset] do
          log("Reached Rate Limit. Goin sleep to end of limit.", :debug)
          sleep_time = limits[:reset] - :os.system_time(:millisecond)
          {state, sleep_time}
        else
          log("Url is forbidden #{result.request_url}", :debug)
          %{state | queue: head}
        end

      nil ->
        state

      _ ->
        log(
          " Undefined error in process_queue/1 on element.id #{element.id}, element.path #{
            element.path
          }",
          :error
        )

        %{state | queue: head}
    end
  end

  defp send_request(path) do
    result =
      HTTPoison.get!(path, [],
        timeout: 5_000,
        recv_timeout: 5_000,
        hackney: [pool: false] |> join_auth_option,
        # Resourse url can be moved to another, follow up to 5 times (as default :max_redirect)
        follow_redirect: true,
        ssl: [{:versions, [:"tlsv1.2"]}]
      )

    case result do
      %HTTPoison.Response{status_code: 200, body: body} ->
        {:ok, Poison.decode!(body), result}

      %HTTPoison.Response{status_code: 404} ->
        {:not_found, "Given url not found (404)", result}

      %HTTPoison.Response{status_code: 403, body: body} ->
        {:forbidden, Poison.decode!(body), result}

      %HTTPoison.Response{status_code: code, body: body} ->
        {:error, "Unknown status code #{code}, body: #{body}", result}
    end
  end

  defp update_element(element, repository, master_data, response) do
    with commit when not is_nil(commit) <- master_data["commit"],
         commiter when not is_nil(commiter) <- commit["committer"],
         commit_date when not is_nil(commit_date) <- commiter["date"],
         stars when not is_nil(stars) <- repository["stargazers_count"] do
      prepare = %{
        last_commit_date: commit_date,
        stars: stars,
        fetch_time: DateTime.utc_now()
      }

      to_update =
        if element.path != repository["html_url"] do
          # If URL of element and URL of github repository data is not same
          # This means repository moved to another url
          # Also can remove slash on end of url
          log(
            "update_element element.path (#{element.path}) is not same with repository[\"html_url\"] (#{
              repository["html_url"]
            }), set new element path ",
            :debug
          )

          Map.put(prepare, :path, repository["html_url"])
        else
          prepare
        end

      result = Funbox.Awesome.update_repo(element, to_update)
      {:ok, result}
    else
      nil -> log("Catched nil in update_element/4", :error)
      _ -> log("Undefined error update_element/4", :error)
    end
  end

  # Example of headers
  # {"X-RateLimit-Limit", "60"}
  # {"X-RateLimit-Remaining", "56"}
  # {"X-RateLimit-Reset", "1372700873"}
  defp get_github_limits_from_headers(list) do
    Enum.reduce(list, %{}, fn
      {"X-RateLimit-Limit", val}, acc ->
        case Integer.parse(val) do
          {int, _} ->
            Map.put(acc, :limit, int)

          _ ->
            acc
        end

      {"X-RateLimit-Remaining", val}, acc ->
        case Integer.parse(val) do
          {int, _} ->
            Map.put(acc, :remain, int)

          _ ->
            acc
        end

      {"X-RateLimit-Reset", val}, acc ->
        case Integer.parse(val) do
          {int, _} ->
            # Convert to milliseconds
            Map.put(acc, :reset, int * 1000)

          _ ->
            acc
        end

      _, acc ->
        acc
    end)
  end

  defp join_auth_option(options) do
    {:ok, name} = :application.get_application(__MODULE__)
    config = Application.get_env(name, __MODULE__)

    if config && config[:github_username] && config[:github_token] do
      Keyword.put_new(options, :basic_auth, {config[:github_username], config[:github_token]})
    else
      options
    end
  end

  defp get_repo_path_from_uri(url) do
    Regex.run(~r/(?<=https:\/\/github.com)(\/[\w\d-_\.]+\/[\w\d-_\.]+)/, url)
  end

  defp log(message, level \\ :info) do
    case level do
      :info ->
        Logger.info("#{__MODULE__} " <> message, backend: :github_monitor_log)

      :debug ->
        Logger.debug("#{__MODULE__} " <> message, backend: :github_monitor_log)

      :error ->
        Logger.error("#{__MODULE__} " <> message, backend: :github_monitor_log)
    end
  end
end
