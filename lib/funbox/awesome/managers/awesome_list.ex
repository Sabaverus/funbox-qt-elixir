defmodule Funbox.Awesome.Managers.AwesomeList do
  @moduledoc false

  import HTTPoison.Response
  import Funbox.Awesome.Parser

  require Logger

  def get_content_and_parse(remote_path) do
    Logger.debug("Sheduler \"FetchRepos\" started")

    case get_content(remote_path) do
      {:ok, body, _response} ->
        body
        |> Funbox.Awesome.Parser.parse_content()
        |> Funbox.Awesome.Parser.process_content()
        |> Funbox.Awesome.Parser.push_parsed_content()

      {:error, message, _response} ->
        Logger.error("#{__MODULE__} " <> message)
    end

    Logger.debug("Sheduler \"FetchRepos\" ended")
  end

  defp get_content(remote_path) do
    response =
      HTTPoison.get!(remote_path, [],
        timeout: 5_000,
        recv_timeout: 5_000,
        hackney: [pool: false],
        ssl: [{:versions, [:"tlsv1.2"]}]
      )

    case response do
      %HTTPoison.Response{status_code: 200, body: body} ->
        {:ok, body, response}

      %HTTPoison.Response{status_code: 404} ->
        {:error, "Status code 404 on given url", response}

      %HTTPoison.Response{status_code: code, body: body} ->
        {:error, "Unexpected status code #{code}, body: #{body}", response}
    end
  end
end
