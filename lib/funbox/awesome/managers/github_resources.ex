defmodule Funbox.Awesome.Managers.GithubResources do
  @moduledoc false

  alias Funbox.Awesome.Resources.Resource, as: Resource
  alias Funbox.Repo, as: DB

  @doc """
  Get list of Funbox.Awesome.Resources.Resource 
  with type == 1 and fetch_time older than one day
  push each of these to GithubWorker in queue for update
  """
  def manage_github_resources() do
    time = DateTime.utc_now() |> DateTime.add(-24 * 60 * 60, :second)

    repositories =
      Resource
      |> Resource.get_github_resouses()
      |> Resource.fetch_time_older(time)
      |> Resource.exclude_with_errors()
      |> DB.all()

    current_queue = get_current_queue()

    Enum.map(repositories, fn repo ->
      add_to_queue(repo, current_queue)
    end)
  end

  defp add_to_queue(repo, queue) do
    # Add repository in queue only if queue doesn't have this element
    if is_nil(Enum.find(queue, nil, &(&1.id == repo.id))) do
      GenServer.call(GithubWorker, {:task, repo})
    end
  end

  defp get_current_queue() do
    GenServer.call(GithubWorker, {:get_queue})
  end
end
