defmodule Funbox.Awesome.Parser.Structs.Element do
  @moduledoc false

  defstruct [:title, :description, :path, :type]

  defp get_regex(), do: ~r/\[(.+)\]\((.+)\) - (.+)/

  def parse_markdown_text(text) do
    [^text, title, link, desc] = Regex.run(get_regex(), text)

    %Funbox.Awesome.Parser.Structs.Element{
      title: title,
      description: desc,
      path: link,
      type: get_link_type(link)
    }
  end

  @doc """
  Checks given string by regex pattern and returns true on match
  """
  def is_element?(text) do
    String.match?(text, get_regex())
  end

  defp get_link_type(link) do
    if String.match?(link, ~r/^https\:\/\/github.com\/.+\/.+/) do
      1
    else
      2
    end
  end
end
