defmodule Funbox.Awesome.Parser.Structs.Section do
  @moduledoc false

  defstruct [:title, description: "", items: []]

  def new(title) do
    %Funbox.Awesome.Parser.Structs.Section{
      title: title
    }
  end

  def add_items(section, items) do
    %{section | items: items}
  end

  def add_description(section, [desc | _]) do
    %{section | description: desc}
  end
end
