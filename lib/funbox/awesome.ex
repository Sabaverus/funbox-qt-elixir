defmodule Funbox.Awesome do
  @moduledoc """
  The Awesome context.
  """

  alias Funbox.Repo, as: DB
  alias Funbox.Awesome.Resources.Resource, as: Resource
  alias Funbox.Awesome.Resources.ResourceSection, as: ResourceSection

  @doc """
  Returns the list of Resources.
  """
  def list_repos do
    DB.all(Resource)
  end

  @doc """
  Returns the list of repos sections.
  """
  def get_list_sections(filters \\ []) do
    ResourceSection
    |> ResourceSection.sort_by_title(:asc)
    |> ResourceSection.left_join_resources()
    |> Resource.set_filters(filters)
    |> ResourceSection.preload_resources()
    |> DB.all()
  end

  @doc """
  Gets a single resource.

  Raises `Ecto.NoResultsError` if the Resource does not exist.

  ## Examples

      iex> get_resource!(123)
      %Resource{}

      iex> get_resource!(456)
      ** (Ecto.NoResultsError)

  """
  def get_resource!(id), do: DB.get!(Resource, id)

  @doc """
  Creates a resource.

  ## Examples

      iex> create_resource(%{field: value})
      {:ok, %Resource{}}

      iex> create_resource(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_resource(attrs \\ %{}) do
    %Resource{}
    |> Resource.changeset(attrs)
    |> DB.insert()
  end

  @doc """
  Updates a resource.

  ## Examples

      iex> update_resource(resource, %{field: new_value})
      {:ok, %Resource{}}

      iex> update_resource(resource, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_resource(%Resource{} = resource, attrs) do
    resource
    |> Resource.changeset(attrs)
    |> DB.update()
  end

  @doc """
  Deletes a Resource.

  ## Examples

      iex> delete_resource(resource)
      {:ok, %Resource{}}

      iex> delete_resource(resource)
      {:error, %Ecto.Changeset{}}

  """
  def delete_resource(%Resource{} = resource) do
    DB.delete(resource)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking resource changes.

  ## Examples

      iex> change_resource(resource)
      %Ecto.Changeset{source: %Resource{}}

  """
  def change_resource(%Resource{} = resource) do
    Resource.changeset(resource, %{})
  end

  def get_resource_section!(id), do: DB.get(ResourceSection, id)

  @doc """
  Returns an `%Resource`
  """
  def get_resource_section_by_code(code) do
    ResourceSection
    |> ResourceSection.by_code(code)
    |> DB.all()
  end

  @doc """
  
  """
  def create_resource_section(attrs \\ %{}) do
    %ResourceSection{}
    |> ResourceSection.changeset(attrs)
    |> DB.insert()
  end
end
