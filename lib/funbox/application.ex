defmodule Funbox.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Funbox.Repo,
      # Start the endpoint when the application starts
      FunboxWeb.Endpoint,
      # Starts a worker by calling: Funbox.Worker.start_link(arg)
      # {Funbox.Worker, arg},
      worker(Funbox.Scheduler, []),
      worker(Funbox.Awesome.Workers.GithubWorker, [GithubWorker])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Funbox.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    FunboxWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
