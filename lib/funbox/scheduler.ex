defmodule Funbox.Scheduler do
  @moduledoc false
  use Quantum.Scheduler,
    otp_app: :funbox
end
