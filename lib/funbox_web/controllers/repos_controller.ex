defmodule FunboxWeb.ReposController do
  use FunboxWeb, :controller

  alias Funbox.Awesome

  def index(conn, params) do
    default_filter = %{
      :type => {:==, :type, 1}
    }

    filters =
      Enum.reduce(params, default_filter, fn
        {"min_stars", value}, acc ->
          case Integer.parse(value) do
            {min_stars, _} ->
              if min_stars > 0 && min_stars < 9_999_999 do
                Map.put(acc, :stars, {:>, :stars, min_stars})
              end

            :error ->
              acc
          end

        {"type", _value}, acc ->
          Map.put(acc, :type, {:>, :type, 0})

        _, acc ->
          acc
      end)

    repos_list = filters |> Map.values() |> Awesome.get_list_sections()

    render(conn, "index.html", repos_list: repos_list)
  end
end
