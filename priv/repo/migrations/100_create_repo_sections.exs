defmodule Funbox.Repo.Migrations.CreateRepoSections do
  use Ecto.Migration

  def change do
    create table(:repo_sections) do
      add :title, :string
      add :description, :string
      add :code, :string

      timestamps()
    end

    create unique_index(:repo_sections, [:code])
  end
end
