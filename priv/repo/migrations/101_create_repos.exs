defmodule Funbox.Repo.Migrations.CreateRepos do
  use Ecto.Migration

  def change do
    create table(:repos) do
      add :title, :string
      add :description, :string
      add :path, :string
      add :section, references(:repo_sections)
      add :type, :integer
      add :stars, :integer
      add :fetch_time, :utc_datetime
      add :error_code, :integer
      add :last_commit_date, :utc_datetime

      timestamps()
    end

    create unique_index(:repos, [:title])
  end
end
