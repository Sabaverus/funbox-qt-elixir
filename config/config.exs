# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :funbox, Funbox.Scheduler,
  jobs: [
    fetch_repos: [
      schedule: "0 0 * * *",
      task: {
        Funbox.Awesome.Managers.AwesomeList,
        :get_content_and_parse,
        [
          "https://raw.githubusercontent.com/h4cc/awesome-elixir/master/README.md"
        ]
      }
    ],
    update_repos: [
      schedule: "0 */1 * * *",
      task: {
        Funbox.Awesome.Managers.GithubResources,
        :manage_github_resources,
        []
      }
    ]
  ]

config :funbox,
  ecto_repos: [Funbox.Repo]

# Configures the endpoint
config :funbox, FunboxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ut5GHczvXLfsXulDC5X+45kr7bmvho/LtS3x5sYJv+5924NU+1PcA7m7NTcuoj0b",
  render_errors: [view: FunboxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Funbox.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, backends: [{LoggerFileBackend, :error_log}, :console]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :logger, :error_log,
  path: "tmp/error.log",
  level: :error

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
