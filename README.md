# Funbox qt-elixir

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

В целом проект запускается и работает самостоятельно. Как то конкретно запускать не нужно, ничем не отличается от гайда выше.

После запуска автоматически встанут в cron-like(Quantum) модули:
  * Funbox.Awesome.Managers.AwesomeList - ежедневное обновление списка репозиториев
  * Funbox.Awesome.Managers.GithubResouses - ежечасная проверка на ресурсы, которые необходимо обновить
Так-же запустится сервер обновления репозиториев
  * Funbox.Awesome.Workers.GithubWorker (GenServer) - очереди-подобный процесс, который принимает элементы на обновления из крон задачи `Funbox.Awesome.Managers.GithubResouses.manage_github_resourses`, который, с интервалом `Funbox.Awesome.Workers.GithubWorker@loop_threshold`(по умолчанию 100ms) тянет из очереди элемент на обновление
  данных с github. 

Funbox.Awesome.Workers.GithubWorker по умолчанию может работать без авторизации в апи гитхаба, но там серьезное ограничение, которое не позволит обновить больше 30 элементов в час, при достижении которого сервер "уснет"(Process.send_after) на оставшееся время до истечения лимита.
Но можно указать в конфиге параметры авторизации
config :funbox, Funbox.Awesome.Workers.GithubWorker, 
  github_username:  "",
  github_token:     ""
В dev.exs конфиге уже забиты рабочие значения логина и токена. Можно использовать их, не жалко. Ограничение на функционал токена выставлены.

Периоды Funbox.Awesome.Managers.AwesomeList и Funbox.Awesome.Managers.GithubResouses так-же могут управляться в конфиге config.exs

В случае, если не хочется ждать ночи, когда будет вытянут список репозиториев, можно дернуть вручную из консоли проекта(!)
Funbox.Awesome.Managers.AwesomeList.get_content_and_parse("https://raw.githubusercontent.com/h4cc/awesome-elixir/master/README.md")

